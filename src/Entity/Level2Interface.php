<?php

namespace Drupal\atinternet\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Level2 entity entities.
 */
interface Level2Interface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
